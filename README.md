SSO-LQDN
=========

Rôle permettant d'installer un serveur de SSO basé sur Keycloak.

Ce rôle se charge d'installer Keycloak. À terme, il se chargera aussi de faire la mise à jour.

Il ne se charge pas d'ajouter des utilisateurices, ni de configurer les intégrations avec les services tiers.

Ce rôle va utiliser la configuration "high availability" pour Keycloack par défault.

À noter qu'il utilise MariaDB au lieu de postgreSQL. 

Requirements
------------

Tout est inclus dans ce rôle. Il est fortement inspiré par les rôles suivants :

- https://github.com/em0lar/ansible-role-keycloak-postgres


# vars file for sso-lqdn

# General user setup

keycloak_service_group: "keycloak"
keycloak_service_user: "keycloak"
keycloak_service_name: "keycloak"
keycloak_base_path: "/opt/keycloak"
keycloak_dest: "{{ keycloak_base_path }}"

## General settings
keycloak_version: "12.0.4"
keycloak_url: "https://github.com/keycloak/keycloak/releases/download/{{ keycloak_version }}/keycloak-{{ keycloak_version }}.zip"
keycloak_archive: "keycloak-{{ keycloak_version }}.zip"
keycloak_archive_on_target: True # To download the archive directly to the server

# Database settings
keycloak_mysql_root_password: "{{ vault_keycloak_mysql_root_password}}"
keycloak_db_name: "keycloak"
keycloak_db_admin: "keycloak"
keycloak_db_pwd: "{{ vault_keycloak_db_pwd }}"

# More General settings ( Optional )
keycloak_jboss_home: "{{ keycloak_base_path }}/keycloak-{{ keycloak_version }}"
keycloak_config_dir: "{{ keycloak_jboss_home }}/standalone/configuration"

# If you want to create the admin user
keycloak_create_admin: false
keycloak_admin_user: "admin"
keycloak_admin_password: "{{ vault_keycloak_admin_password }}"

## Networking
keycloak_behind_reverseproxy: true
keycloak_bind_address: "127.0.0.1"
keycloak_http_port: "8080"
keycloak_https_port: "8443"
keycloak_management_http_port: "9990"
keycloak_management_https_port: "9993"

# Run settings
keycloak_startup_timeout: "300"
keycloak_java_opts: "-Xms256m -Xmx1024m"

# In case you want to force the re installation
keycloak_force_install: false

## Customization
keycloak_profile_preview: false
keycloak_welcome_theme: "keycloak"

mysql_credential_file:
  debian: '/etc/mysql/debian.cnf'

### Export old user data from Widly Installations ;

`bin/standalone.sh -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=dir -Dkeycloak.migration.dir=/root/keycloak-user/`

Dependencies
------------

Tout est inclus dans ce rôle.

Example Playbook
----------------

    - hosts: lqdn-sso
      roles:
         - sso-lqdn

License
-------

BSD

Author Information
------------------

Fait chez LQDN ( LQDN.fr ) par nono ( np@laquadrature.net )
