---
- name: Checking for existing installation of the same version
  stat:
    path: "{{ keycloak_jboss_home }}"
  register: existing_deploy

- block:
    - name: Stopping the existing keycloak service
      systemd:
        name: "{{ keycloak_service_name }}"
        state: stopped
      ignore_errors: true

    - name: Removing the existing Keycloak deployment
      file:
        path: "{{ keycloak_jboss_home }}"
        state: absent
  when: ( existing_deploy.stat.exists | bool ) and ( keycloak_force_install | bool )

- name: Checking for an existing deployment after possible forced removal
  stat:
    path: "{{ keycloak_jboss_home }}"
  register: existing_deploy

- name: Create Keycloak install location
  file:
    dest: "{{ keycloak_base_path }}"
    state: directory
    owner: "{{ keycloak_service_user }}"
    group: "{{ keycloak_service_group }}"
  become: yes

- block:
    - name: Download Keycloak archive to target
      get_url:
        url: "{{ keycloak_url }}"
        dest: "{{ keycloak_dest }}"
        owner: "{{ keycloak_service_user }}"
        group: "{{ keycloak_service_group }}"

    - name: Extract Keycloak archive on target
      unarchive:
        remote_src: yes
        src: "{{ keycloak_dest }}/{{ keycloak_archive }}"
        dest: "{{ keycloak_dest }}"
        creates: "{{ keycloak_jboss_home }}"
        owner: "{{ keycloak_service_user }}"
        group: "{{ keycloak_service_group }}"
      notify:
        - restart keycloak
  become: yes
  when: keycloak_archive_on_target

- block:
    - name: Download Keycloak archive to local
      delegate_to: localhost
      get_url:
        url: "{{ keycloak_url }}"
        dest: "{{ keycloak_local_download_dest }}/{{ keycloak_archive }}"

    - name: extract Keycloak archive on local
      become: yes
      unarchive:
        remote_src: no
        src: "{{ keycloak_local_download_dest }}/{{ keycloak_archive }}"
        dest: "{{ keycloak_dest }}"
        creates: "{{ keycloak_jboss_home }}"
        owner: "{{ keycloak_service_user }}"
        group: "{{ keycloak_service_group }}"
      notify:
        - restart keycloak
  when: not keycloak_archive_on_target

- name: Add systemd unit file for keycloak service
  become: yes
  template:
    src: "keycloak.service.j2"
    dest: "/etc/systemd/system/{{ keycloak_service_name }}.service"
    owner: root
    group: root
    mode: 0644
  notify:
    - reload systemd
    - restart keycloak
